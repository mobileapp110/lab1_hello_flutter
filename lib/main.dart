import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String chinessGreeting = "Nihao Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title: Text("Hello Flutter"),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = englishGreeting;
                    });
                  },
                  icon: Icon(Icons.label)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = chinessGreeting;
                    });
                  },
                  icon: Icon(Icons.apple)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      displayText = spanishGreeting;
                    });
                  },
                  icon: Icon(Icons.receipt))
            ],
          ),
          body: Center(
            child: Text(
              displayText,
              style: TextStyle(fontSize: 40),
            ),
          )),
    );
  }
}
